Setup a simple scene with a Avatar in the form of mario. 
User can use the joystick for moving mario around. 
If he runs over a light it will turn on for 5 seconds, he can pickup coins. 
The Chest shoots coins out, uses a Object pool to spawn coins and a coroutine to spawn them.
Coins will disapear after a set duration if not picked up.
I imported the Mario model , attached some animations from the Unity Standard Assets.
The joystick asset is from the asset store. 
The user can touch mario to show a speech bubble, which will display a random message and then disapear after 5 secodns
