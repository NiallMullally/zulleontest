﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public float intialSpawnDelay = 2.0f;
    public float delayBetweenSpawn = 0.1f;

    public void triggerSpawningCoins()
    {
        StartCoroutine("spawnCoins");
    }

    IEnumerator spawnCoins()
    {
        yield return new WaitForSeconds(intialSpawnDelay);
        while (true)
        {
            GameObject obj = CoinObjectPool.sInstance.getNewObject();
            obj.SetActive(true);
            obj.GetComponent<Rigidbody>().velocity = new Vector3(10, 2, 0);
            obj.transform.position = this.transform.position;
            yield return new WaitForSeconds(delayBetweenSpawn);
        }
    }
}
