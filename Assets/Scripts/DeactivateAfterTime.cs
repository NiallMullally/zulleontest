﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateAfterTime : MonoBehaviour
{
    public float timeBeforeDeactivation = 5.0f;

    void Start()
    {
        StopCoroutine("deactivateAfterTime");
        StartCoroutine("deactivateAfterTime", timeBeforeDeactivation);
    }

    IEnumerator deactivateAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        gameObject.SetActive(false);
    }
}
