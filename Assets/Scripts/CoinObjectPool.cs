﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Setup object pool which is used to retrieve our coins, all setup is done at the start to prevent potential loading issues during gameplay

public class CoinObjectPool : MonoBehaviour
{
    public static CoinObjectPool sInstance;

    public GameObject spawnLocation;
    public GameObject prefabToSpawn;
    public int mInitialNumCoins = 20;

    private List<GameObject> objectPool;

    // Start is called before the first frame update
    void Awake()
    {
        sInstance = this;
        setupObjects();
    }

    public GameObject getNewObject()
    {
        foreach(GameObject gameObject in objectPool)
        {
            if (gameObject.activeSelf == false)
            {
                return gameObject;
            }
        }

        // Should never reach this point, but in the event that theres not enough game objects we need to create a new one 
        GameObject newObj = createNewObject();
        objectPool.Add(newObj);
        return newObj;
    }

    void setupObjects()
    {
        objectPool = new List<GameObject>();

        for (int i = 0; i < mInitialNumCoins; i++)
        {
            objectPool.Add(createNewObject());
        }
    }

    GameObject createNewObject()
    {
        GameObject obj = Instantiate(prefabToSpawn, Vector3.zero, Quaternion.identity) as GameObject;
        obj.SetActive(false);
        obj.transform.parent = spawnLocation.transform;
        return obj;
    }
}
