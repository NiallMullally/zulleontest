﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public bool shouldRotateAround;
    public GameObject targetObject;
    public float idleRotationSpeed = 1.0f;

    public float cameraDistance = 8.0f;
    public float cameraHeight = 1.5f;

    public float cameraRotationDamping = 2.0f;
    public float cameraHeightDamping = 1.0f;

    // If in the menu or user hasnt moved, the camera will rotate around the character
    // otherwise it uses a smooth rotation to follow the character.

    private void LateUpdate()
    {
        if (shouldRotateAround)
        {
            transform.LookAt(targetObject.transform);
            transform.Translate(Vector3.right * (idleRotationSpeed * Time.deltaTime));
        }
        else
        {
            float desiredAngle = targetObject.transform.eulerAngles.y;
            float desiredHeight = targetObject.transform.position.y + cameraHeight;

            float currentRotation = transform.eulerAngles.y;
            float currentHeight = transform.position.y;

            currentRotation = Mathf.LerpAngle(currentRotation, desiredAngle, cameraRotationDamping * Time.deltaTime);
            currentHeight = Mathf.Lerp(currentHeight, desiredHeight, cameraHeightDamping * Time.deltaTime);

            Quaternion rotation = Quaternion.Euler(0.0f, currentRotation, 0.0f);

            transform.position = targetObject.transform.position - (rotation * Vector3.forward * cameraDistance);
            transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

            transform.LookAt(targetObject.transform);
        }
    }

    public void shouldRotateAroundCharacter(bool enable)
    {
        shouldRotateAround = enable;
    }
}
