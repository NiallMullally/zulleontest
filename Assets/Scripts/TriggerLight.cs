﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(Light))]

public class TriggerLight : MonoBehaviour
{
    public ParticleSystem mParticleSystem;
    public Light mLight;

    public bool mIsLightOn = false;
    public float mDelayBetweenActivations = 5.0f;
    private float mTimer = 0;
    // Start is called before the first frame update
    void Start()
    {
        mParticleSystem.Stop();
        mLight.enabled = false;
    }

    private void FixedUpdate()
    {
        if (mIsLightOn)
        {
            mTimer += Time.deltaTime;

            if (mTimer >= mDelayBetweenActivations)
            {
                mTimer = 0.0f;
                toggleLight(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (mIsLightOn == false)
        {
            toggleLight(true);
        }
    }

    private void toggleLight(bool enabled)
    {
        mIsLightOn = enabled;
        mLight.enabled = enabled;

        if (enabled)
        {
            mParticleSystem.Play();
        }
        else
        {
            mParticleSystem.Stop();
        }
    }
}
