﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Character needs the following otherwise it should error.
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Joystick))]
[RequireComponent(typeof(CapsuleCollider))]

public class Character : MonoBehaviour
{
    Animator mAnimator;
    Rigidbody mRigidBody;
    CapsuleCollider mColider;
    public Joystick mJoyStick;

    // Start is called before the first frame update
    void Start()
    {
        mAnimator = GetComponent<Animator>();
        mRigidBody = GetComponent<Rigidbody>();
        mColider = GetComponent<CapsuleCollider>();
    }

    void Update()
    {
        handleMovement();
    }

    void updateAnimator()
    {
        float horizontal = Mathf.Abs(mJoyStick.Horizontal);
        float Vertical = mJoyStick.Vertical;

        float highestValue = horizontal  > Vertical ? horizontal : Vertical;

        mAnimator.SetFloat("Turn", mJoyStick.Horizontal);
        mAnimator.SetFloat("Forward", highestValue);
    }

    private void handleMovement()
    {
        print("Horizontal " + mJoyStick.Horizontal + " : : Vertical %f" + mJoyStick.Vertical);
        updateAnimator();
    }
}
