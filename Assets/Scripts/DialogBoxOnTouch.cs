﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class DialogBoxOnTouch : MonoBehaviour
{
    public SpeachBubble mSpeachBubble;
    Vector2[] mTouches = new Vector2[5];

    // Update is called once per frame
    void Update()
    {
        if (GameManager.sInstance.mHasGameStarted && Input.touchCount > 0)
        {
            foreach (Touch t in Input.touches)
            {
                mTouches[t.fingerId] = Camera.main.ScreenToWorldPoint(Input.GetTouch(t.fingerId).position);

                if (Input.GetTouch(t.fingerId).phase == TouchPhase.Began)
                {
                    Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(t.fingerId).position);
                    RaycastHit hit;
                    if (Physics.Raycast(raycast, out hit))
                    {
                        if (hit.collider.tag == "Player")
                        {
                            mSpeachBubble.displayRandomText();
                        }
                    }
                }
            }
        }
    }
}