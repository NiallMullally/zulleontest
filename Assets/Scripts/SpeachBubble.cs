﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Simple script to set the dialog on the speach bubble, using Serialize field to show
// how private variables can be made public in the inspector
public class SpeachBubble : MonoBehaviour
{
    [SerializeField]
    List<string> mRandomLines = new List<string>();

    [SerializeField]
    TextMeshProUGUI mText;

    [SerializeField]
    Canvas mCanvas;

    // Start is called before the first frame update
    void Start()
    {
        mCanvas.enabled = false;
    }

    public void displayRandomText()
    {
        mCanvas.enabled = true;
        mText.text = mRandomLines[Random.Range(0, mRandomLines.Count)];

        StopCoroutine("disableDialogAfterTime");
        StartCoroutine("disableDialogAfterTime", 5);
    }

    IEnumerator disableDialogAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        mCanvas.enabled = false;
    }
}
