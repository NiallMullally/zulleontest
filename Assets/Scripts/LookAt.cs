﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    public bool reverse;
    public GameObject targetObject;
    // Start is called before the first frame update
    private void LateUpdate()
    {
        if (reverse)
        {
            transform.LookAt(2 * transform.position - targetObject.transform.position);
        }
        else
        {
            transform.LookAt(targetObject.transform);
        }

    }
}
