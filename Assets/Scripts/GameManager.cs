﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager sInstance;

    public GameObject mStartMenu;
    public GameObject mJoyStick;
    public GameObject mCoinSpawner;
    public bool mHasGameStarted;

    // Start is called before the first frame update
    void Start()
    {
        sInstance = this;
        Camera.main.GetComponent<CameraFollow>().shouldRotateAroundCharacter(true);
        mStartMenu.SetActive(true);
        mJoyStick.SetActive(false);
        mHasGameStarted = false;
    }

    public void startGame()
    {
        mHasGameStarted = true;
        mJoyStick.SetActive(true);
        mStartMenu.SetActive(false);

        Camera.main.GetComponent<CameraFollow>().shouldRotateAroundCharacter(false);
        mCoinSpawner.GetComponent<CoinSpawner>().triggerSpawningCoins();
    }
}
